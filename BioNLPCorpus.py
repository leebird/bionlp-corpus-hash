import glob
import os
import pprint

class BioNLPCorpus:

    textFiles = []

    corpusPath = ''

    corpusHash = {}

    def scan(self):
        files = glob.glob(self.corpusPath + '/*.txt')
        for f in files:
            textName = os.path.basename(f)
            textName = textName[:-4]
            self.textFiles.append(textName)
            self.corpusHash[textName] = {'Name':textName,
                                         'Entity':{},
                                         'Trigger':{},
                                         'Relation':{}}
            
    def map_entity(self):
        for textFile in self.textFiles:
            entityPath = self.corpusPath + '/' + textFile + '.a1'
            f = open(entityPath,'r')
            for line in f:
                line = line.strip()
                fields = line.split('\t')
                entityInfo = fields[1].split(' ')
                
                entityId = fields[0]
                entityText = fields[2]
                entityType = entityInfo[0]
                entityStart = entityInfo[1]
                entityEnd = entityInfo[2]
                
                self.corpusHash[textFile]['Entity'][entityId] = {}
                self.corpusHash[textFile]['Entity'][entityId]['ID'] = entityId
                self.corpusHash[textFile]['Entity'][entityId]['Text'] = entityText
                self.corpusHash[textFile]['Entity'][entityId]['Type'] = entityType
                self.corpusHash[textFile]['Entity'][entityId]['Start'] = entityStart
                self.corpusHash[textFile]['Entity'][entityId]['End'] = entityEnd
                
    def map_relation(self):
        for textFile in self.textFiles:
            relationPath = self.corpusPath + '/' + textFile + '.a2'
            f = open(relationPath,'r')
            for line in f:
                line = line.strip()
                relationType = line[0];

                fields = line.split('\t')
                
                if relationType == 'T':
                    triggerInfo = fields[1].split(' ')
                    triggerId = fields[0]
                    triggerText = fields[2]
                    triggerType = triggerInfo[0]
                    triggerStart = triggerInfo[1]
                    triggerEnd = triggerInfo[2]                
                    self.corpusHash[textFile]['Trigger'][triggerId] = {}
                    self.corpusHash[textFile]['Trigger'][triggerId]['ID'] = triggerId
                    self.corpusHash[textFile]['Trigger'][triggerId]['Text'] = triggerText
                    self.corpusHash[textFile]['Trigger'][triggerId]['Type'] = triggerType
                    self.corpusHash[textFile]['Trigger'][triggerId]['Start'] = triggerStart
                    self.corpusHash[textFile]['Trigger'][triggerId]['End'] = triggerEnd

                elif relationType == 'E':
                    relationId = fields[0]                    
                    relationInfo = fields[1].split(' ')
                    
                    relationType = relationInfo[0].split(':')
                    relationTypeId = relationType[1]
                    relationTypeText = relationType[0]

                    self.corpusHash[textFile]['Relation'][relationId] = {}
                    self.corpusHash[textFile]['Relation'][relationId]['ID'] = relationId
                    self.corpusHash[textFile]['Relation'][relationId]['TriggerID'] = relationTypeId
                    self.corpusHash[textFile]['Relation'][relationId]['Type'] = relationTypeText

                    for arg in relationInfo[1:]:
                        argInfo = arg.split(':')
                        argType = argInfo[0]
                        argId = argInfo[1]
                        self.corpusHash[textFile]['Relation'][relationId][argType] = argId


    ''' 
    Genia corpus uses index to annotate entity/event 
    in the text. Sometimes we want to tag them in the
    sentence and get that sentence only.
    indices is a list of <start,end,tag> pairs. Overlapping
    is not allowed between pairs, e.g., <10,20,'Theme'> and 
    <15,25,'Cause'> can't appear in the list at the same time.
    If indices are across sentences, all the sentences will 
    be returned.

    Auguments:
    text: original text
    sentences: splitted sentences of text
    indices: the index and tag you want to tag in the sentences
    '''
    def tag_sentence(self,text,sentences,indices):
        segments = []
        taggedText = []
        targetSentenceId = []
        taggedSentence = ''
        pointer = 0

        indices = sorted(indices,key=lambda a:a[0])

        for sentenceId,sentence in enumerate(sentences):
            indexStart = text.index(sentence)
            indexEnd = indexStart + len(sentence)
            for index in indices:
                minStart = index[0]
                if indexStart <= minStart and indexEnd >= minStart:
                    if sentenceId not in targetSentenceId:
                        targetSentenceId.append(sentenceId)        

        firstSentence = sentences[targetSentenceId[0]]
        lastSentence = sentences[targetSentenceId[-1]]

        firstIndex = text.index(firstSentence)
        lastIndex = text.index(lastSentence) + len(lastSentence)

        targetSentence = text[firstIndex:lastIndex]
        targetSentenceId = ','.join(map(str,targetSentenceId))

        for index in indices:
            tag = index[2]
            senStart = index[0] - firstIndex
            senEnd = index[1] - firstIndex
            segments.append(targetSentence[pointer:senStart])
            taggedText.append('<'+tag+'>'+targetSentence[senStart:senEnd]+'</'+tag+'>')
            pointer = senEnd
        
        for segment,tagged in zip(segments,taggedText):
            taggedSentence += segment + tagged
            
        taggedSentence += targetSentence[pointer:lastIndex]

        return (taggedSentence,targetSentenceId)

    def get_hash(self):
        return self.corpusHash

    def __init__(self,path):
        self.corpusPath = path
        
