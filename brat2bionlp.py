from BioNLPCorpus import BioNLPCorpus

'''
An example using class BioNLPCorpus
This script translates annotation file
in brat format (.ann) into BioNLP format 
(.a1,.a2)

BioNLP format: 
https://sites.google.com/site/bionlpst/home/file-formats

brat format: 
http://brat.nlplab.org/standoff.html

Before running this script,
1. put all .txt file and .ann file into folder 
defined as corpusPath, and change all .ann
files' suffix to .a2
2. make another folder defined as annotationPath
for the new .a1 and .a2 files
'''

corpusPath = 'target/'
annotationPath = 'target_ann/'
corpus = BioNLPCorpus(corpusPath)

corpus.scan()
corpus.map_relation()

corpusHash = corpus.get_hash()

triggerId = []

entityFormat = '{0}\t{1} {2} {3}\t{4}\n'
relationFormat = '{0}\t{1}:{2} {3}:{4}\n'

for name,text in corpusHash.iteritems():

    a1 = ''
    a2 = ''

    a1File = annotationPath + name + '.a1'
    a2File = annotationPath + name + '.a2'

    for relationId,relation in text['Relation'].iteritems():

        if relation['TriggerID'] not in triggerId:
            triggerId.append(relation['TriggerID'])

        try:
            a2 += relationFormat.format(relation['ID'],
                                        relation['Type'],
                                        relation['TriggerID'],
                                        'Cause',
                                        relation['Cause'])
        except KeyError:
            pass

        try:
            a2 += relationFormat.format(relation['ID'],
                                        relation['Type'],
                                        relation['TriggerID'],
                                        'Theme',
                                        relation['Theme'])
        except KeyError:
            pass

    for entityId,entity in text['Trigger'].iteritems():

        if entity['ID'] in triggerId:
            a2 += entityFormat.format(entity['ID'],
                                      entity['Type'],
                                      entity['Start'],
                                      entity['End'],
                                      entity['Text'])
        else:
            a1 += entityFormat.format(entity['ID'],
                                      entity['Type'],
                                      entity['Start'],
                                      entity['End'],
                                      entity['Text'])

    f1 = open(a1File,'w')
    f2 = open(a2File,'w')
    f1.write(a1)
    f2.write(a2)
    f1.close()
    f2.close()

